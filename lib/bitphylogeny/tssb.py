from scipy.misc import logsumexp as log_sum_exp

import networkx as nx
import numpy as np
import sys

import bitphylogeny.util as util

class TSSB(object):
    min_dp_alpha = 0.001

    max_dp_alpha = 10.0
    
    min_dp_gamma = 0.001
    
    max_dp_gamma = 10.0
    
    min_alpha_decay = 0.001
    
    max_alpha_decay = 0.80
    
    def __init__(self, alpha_decay=1.0, data=None, dp_alpha=1.0, dp_gamma=1.0, max_depth=15, min_depth=0, root_node=None):
        if root_node is None:
            raise Exception("Root node must be specified.")
        
        self.min_depth = min_depth
        
        self.max_depth = max_depth
        
        self.dp_alpha = dp_alpha
        
        self.dp_gamma = dp_gamma
        
        self.alpha_decay = alpha_decay
        
        self.data = data
        
        self.num_data = 0 if data is None else data.shape[0]
        
        self.root = {'node' : root_node,
                     'main' : util.bound_beta(1.0, dp_alpha) if self.min_depth == 0 else 0.0,
                     'sticks' : np.empty((0, 1)),
                     'children' : [] }
        
        root_node.tssb = self

        if False:
            data_u = np.random.rand(self.num_data)
            
            self.assignments = []
            
            for n in range(self.num_data):
                (c, _) = self.find_node(data_u[n])
                
                c.add_datum(n)
                
                self.assignments.append(c)
        else:
            self.assignments = []
            
            for n in range(self.num_data):
                self.root['node'].add_datum(n)
                
                self.assignments.append(self.root['node'])
    
    @property
    def nodes(self):
        def descend(root):
            nodes = [root['node']]
    
            for child in root['children']:
                child_nodes = descend(child)
    
                nodes.extend(child_nodes)
    
            return nodes
    
        return descend(self.root)
    
    def add_data(self, data):
        (weights, nodes) = self.get_mixture()
        
        num_new_data = data.shape[0]
        
        for n in range(num_new_data):
            logprobs = []
            
            for k, node in enumerate(nodes):
                logprobs.append(np.log(weights[k]) + node.log_prob(data[n]))
            
            logprobs = np.array(logprobs)
            
            probs = np.exp(logprobs - log_sum_exp(logprobs))
            
            best_k = np.sum(np.random.rand() > np.cumsum(probs))
            
            nodes[best_k].add_datum(n + self.num_data)
            
            self.assignments.append(nodes[best_k])
        
        self.data = np.vstack([self.data, data])
        
        self.num_data += num_new_data

    def clear_data(self):
        dims = self.data.shape[1]
        
        for n in range(self.num_data):
            self.assignments[n].remove_datum(n)
        
        self.assignments = []
        
        self.data = np.empty((0, dims))
        
        self.num_data = 0

    def resample_node_params(self, iters=1):                
        def descend(root):
            for child in root['children']:
                descend(child)
        
            root['node'].resample_parameters()
        
        for _ in range(iters):
            descend(self.root)

    def resample_assignments(self):
        def path_lt(path1, path2):
            if len(path1) == 0 and len(path2) == 0:
                return 0

            elif len(path1) == 0:
                return 1
            
            elif len(path2) == 0:
                return -1
            
            s1 = "".join(map(lambda i: "%03d" % (i), path1))
            
            s2 = "".join(map(lambda i: "%03d" % (i), path2))

            return cmp(s2, s1)

        epsilon = np.finfo(np.float64).eps
        
        lengths = []
        
        reassign = 0
        
        better = 0        
        
        for n in range(self.num_data):
            # Get an initial uniform variate.
            ancestors = self.assignments[n].get_ancestors()
            
            current = self.root
            
            indices = []
            
            for anc in ancestors[1:]:
                index = map(lambda c: c['node'], current['children']).index(anc)
                
                current = current['children'][index]
                
                indices.append(index)
            
            max_u = 1.0
            
            min_u = 0.0
            
            llh_s = np.log(np.random.rand()) + self.assignments[n].log_prob(self.data[n:n + 1])

            while True:
                new_u = (max_u - min_u) * np.random.rand() + min_u
                
                (new_node, new_path) = self.find_node(new_u)
                
                new_llh = new_node.log_prob(self.data[n:n + 1])
                
                if new_llh > llh_s:
                    if new_node != self.assignments[n]:
                        if (new_llh > self.assignments[n].log_prob(self.data[n:n + 1])):
                            better += 1
                        
                        self.assignments[n].remove_datum(n)
                        
                        new_node.add_datum(n)
                        
                        self.assignments[n] = new_node
                        
                        reassign += 1
                    
                    break
                
                elif abs(max_u - min_u) < epsilon:
                    print >> sys.stderr, "Slice sampler shrank down.  Keep current state."
                
                    break
                
                else:
                    path_comp = path_lt(indices, new_path)
                    
                    if path_comp < 0:
                        min_u = new_u
                    
                    elif path_comp > 0:
                        max_u = new_u
                    
                    else:
                        raise Exception("Slice sampler weirdness.")
            
            lengths.append(len(new_path))
        
        lengths = np.array(lengths)

    def cull_tree(self):
        def descend(root):
            counts = np.array(map(lambda child: descend(child), root['children']))
            
            keep = len(np.trim_zeros(counts, 'b'))

            for child in root['children'][keep:]:
                child['node'].kill()
                
                del child['node']

            root['sticks'] = root['sticks'][:keep]
            
            root['children'] = root['children'][:keep]

            return np.sum(counts) + root['node'].num_local_data()

        descend(self.root)

    def resample_sticks(self):
        def descend(root, depth=0):
            data_down = 0
            
            indices = range(len(root['children']))
            
            indices.reverse()
            
            for i in indices:
                child = root['children'][i]
                
                child_data = descend(child, depth + 1)
                
                post_alpha = 1.0 + child_data
                
                post_beta = self.dp_gamma + data_down
                
                root['sticks'][i] = util.bound_beta(post_alpha, post_beta)
                
                data_down += child_data

            # Resample the main break.
            data_here = root['node'].num_local_data()
            
            post_alpha = 1.0 + data_here
            
            post_beta = (self.alpha_decay ** depth) * self.dp_alpha + data_down
            
            root['main'] = util.bound_beta(post_alpha, post_beta) if self.min_depth <= depth else 0.0

            return data_here + data_down

        descend(self.root)

    def resample_stick_orders(self):
        def descend(root, depth=0):
            if not root['children']:
                return
           
            new_order = []
           
            represented = set(filter(lambda i: root['children'][i]['node'].has_data(), range(len(root['children']))))
            
            all_weights = np.diff(np.hstack([0.0, util.sticks_to_edges(root['sticks'])]))
            
            while True:
                if not represented:
                    break

                u = np.random.rand()
                
                while True:
                    sub_indices = filter(lambda i: i not in new_order, range(root['sticks'].shape[0]))
                    
                    sub_weights = np.hstack([all_weights[sub_indices], 1.0 - np.sum(all_weights)])
                    
                    sub_weights = sub_weights / np.sum(sub_weights)
                    
                    index = np.sum(u > np.cumsum(sub_weights))

                    if index == len(sub_indices):
                        root['sticks'] = np.vstack([root['sticks'], util.bound_beta(1, self.dp_gamma)])
                        
                        root['children'].append({'node' : root['node'].spawn(),
                                                 'main': util.bound_beta(1.0, (self.alpha_decay ** (depth + 1)) * self.dp_alpha) if self.min_depth <= (depth + 1) else 0.0,
                                                 'sticks' : np.empty((0, 1)),
                                                 'children' : []})
                        
                        all_weights = np.diff(np.hstack([0.0, util.sticks_to_edges(root['sticks'])]))
                    
                    else:
                        index = sub_indices[index]
                        
                        break
                
                new_order.append(index)
                
                represented.discard(index)

            new_children = []
            
            for k in new_order:
                child = root['children'][k]
                
                new_children.append(child)
                
                descend(child, depth + 1)

            for k in filter(lambda k: k not in new_order, range(root['sticks'].shape[0])):
                root['children'][k]['node'].kill()
                
                del root['children'][k]['node']

            root['children'] = new_children
            
            root['sticks'] = np.zeros((len(root['children']), 1))
        
        descend(self.root)
        
        # Immediately resample sticks.
        self.resample_sticks()

    def resample_hyper_parameters(self, dp_alpha=True, alpha_decay=True, dp_gamma=True):
        def dp_alpha_llh(dp_alpha, alpha_decay):
            def descend(dp_alpha, root, depth=0):
                llh = util.log_beta_pdf(root['main'], 1.0, (alpha_decay ** depth) * dp_alpha) if self.min_depth <= depth else 0.0
                
                for child in root['children']:
                    llh += descend(dp_alpha, child, depth + 1)
                
                return llh            
            
            return descend(dp_alpha, self.root)

        if dp_alpha:
            upper = self.max_dp_alpha
            
            lower = self.min_dp_alpha
            
            llh_s = np.log(np.random.rand()) + dp_alpha_llh(self.dp_alpha, self.alpha_decay)
            
            while True:
                new_dp_alpha = (upper - lower) * np.random.rand() + lower
                
                new_llh = dp_alpha_llh(new_dp_alpha, self.alpha_decay)
                
                if new_llh > llh_s:
                    break
                
                elif new_dp_alpha < self.dp_alpha:
                    lower = new_dp_alpha
                
                elif new_dp_alpha > self.dp_alpha:
                    upper = new_dp_alpha
                
                else:
                    raise Exception("Slice sampler shrank to zero!")
            
            self.dp_alpha = new_dp_alpha

        if alpha_decay:
            upper = self.max_alpha_decay
            
            lower = self.min_alpha_decay
            
            llh_s = np.log(np.random.rand()) + dp_alpha_llh(self.dp_alpha, self.alpha_decay)
            
            while True:
                new_alpha_decay = (upper - lower) * np.random.rand() + lower
                
                new_llh = dp_alpha_llh(self.dp_alpha, new_alpha_decay)
                
                if new_llh > llh_s:
                    break
                
                elif new_alpha_decay < self.alpha_decay:
                    lower = new_alpha_decay
                
                elif new_alpha_decay > self.alpha_decay:
                    upper = new_alpha_decay
                
                else:
                    raise Exception("Slice sampler shrank to zero!")
            
            self.alpha_decay = new_alpha_decay

        def dp_gamma_llh(dp_gamma):
            def descend(dp_gamma, root):
                llh = 0
            
                for i, child in enumerate(root['children']):
                    llh += util.log_beta_pdf(root['sticks'][i], 1.0, dp_gamma)
                    
                    llh += descend(dp_gamma, child)
                
                return llh
            
            return descend(dp_gamma, self.root)

        if dp_gamma:
            upper = self.max_dp_gamma
            
            lower = self.min_dp_gamma
            
            llh_s = np.log(np.random.rand()) + dp_gamma_llh(self.dp_gamma)
            
            while True:
                new_dp_gamma = (upper - lower) * np.random.rand() + lower
            
                new_llh = dp_gamma_llh(new_dp_gamma)
                
                if new_llh > llh_s:
                    break
                
                elif new_dp_gamma < self.dp_gamma:
                    lower = new_dp_gamma
                
                elif new_dp_gamma > self.dp_gamma:
                    upper = new_dp_gamma
                
                else:
                    raise Exception("Slice sampler shrank to zero!")
            
            self.dp_gamma = new_dp_gamma
        
    def draw_data(self, num_data=1, **args):
        self.data = []
        
        self.assignments = []
        
        for n in range(num_data):
            u = np.random.rand()
        
            (node, _) = self.find_node(u)
            
            self.data.append(node.sample(args))
            
            self.assignments.append(node)
            
            node.add_datum(n)
            
            self.num_data += 1
        
        self.data = np.concatenate(self.data)
        
        return self.data

    def resample_data(self, **args):
        for n in range(self.num_data):
            u = np.random.rand()
            
            (node, _) = self.find_node(u)
            
            self.assignments[n].remove_datum(n)
            
            node.add_datum(n)
            
            self.assignments[n] = node
            
            self.data[n] = node.sample(args)[0]

    def find_node(self, u):
        def descend(root, u, depth=0):
            if depth >= self.max_depth:
                return (root['node'], [])
            
            elif u < root['main']:
                return (root['node'], [])
            
            else:
                # Rescale the uniform variate to the remaining interval.
                u = (u - root['main']) / (1.0 - root['main'])
                
                # Perhaps break sticks out appropriately.
                while not root['children'] or (1.0 - np.prod(1.0 - root['sticks'])) < u:
                    root['sticks'] = np.vstack([ root['sticks'], util.bound_beta(1, self.dp_gamma) ])
                    
                    root['children'].append({ 'node' : root['node'].spawn(),
                                              'main' : util.bound_beta(1.0, (self.alpha_decay ** (depth + 1)) * self.dp_alpha) if self.min_depth <= (depth + 1) else 0.0,
                                              'sticks' : np.empty((0, 1)),
                                              'children' : [] })

                edges = 1.0 - np.cumprod(1.0 - root['sticks'])
                
                index = np.sum(u > edges)
                
                edges = np.hstack([0.0, edges])
                
                u = (u - edges[index]) / (edges[index + 1] - edges[index])

                (node, path) = descend(root['children'][index], u, depth + 1)
                
                path.insert(0, index)
                
                return (node, path)
        
        return descend(self.root, u)

    def get_mixture(self):
        def descend(root, mass):
            weight = [ mass * root['main'] ]
        
            node = [ root['node'] ]
            
            edges = util.sticks_to_edges(root['sticks'])
            
            weights = np.diff(np.hstack([0.0, edges]))

            for i, child in enumerate(root['children']):
                (child_weights, child_nodes) = descend(child, mass * (1.0 - root['main']) * weights[i])
                
                weight.extend(child_weights)
                
                node.extend(child_nodes)
            
            return (weight, node)
        
        return descend(self.root, 1.0)

    def complete_data_log_likelihood(self):
        weights, nodes = self.get_mixture();
        
        llhs = []
        
        for i, node in enumerate(nodes):
            if node.num_local_data():
                llhs.append(node.num_local_data() * np.log(weights[i]) + node.data_log_likelihood())
        
        return np.sum(np.array(llhs))
    
    def complete_data_log_likelihood_nomix(self):
        weights, nodes = self.get_mixture();
        
        llhs = []
        
        lln = []
        
        for i, node in enumerate(nodes):
            if node.num_local_data():
                llhs.append(node.data_log_likelihood())
        
                lln.append(node.num_local_data() * np.log(weights[i]))
                
        return (np.sum(np.array(lln)), np.sum(np.array(llhs)))
    
    def unnormalized_posterior_with_hypers(self):
        weights, nodes = self.get_mixture();
        
        llhs = []
        
        for i, node in enumerate(nodes):
            if node.num_local_data():
                llhs.append(node.num_local_data() * np.log(weights[i]) + node.data_log_likelihood() + node.parameter_log_prior()) 
        
        llh = np.sum(np.array(llhs))
        
        def alpha_descend(root, depth=0):
            llh = util.log_beta_pdf(root['main'], 1.0,
                                    (self.alpha_decay ** depth) * self.dp_alpha) if self.min_depth <= depth else 0.0
            
            for child in root['children']:
                llh += alpha_descend(child, depth + 1)
            
            return llh            
        
        weights_log_prob = alpha_descend(self.root)
        
        def gamma_descend(root):
            llh = 0
            
            for i, child in enumerate(root['children']):
                llh += util.log_beta_pdf(root['sticks'][i], 1.0, self.dp_gamma)
                
                llh += gamma_descend(child)
            
            return llh
        
        sticks_log_prob = gamma_descend(self.root)
        
        return llh + weights_log_prob + sticks_log_prob
    
    def unnormalized_posterior(self):
        weights, nodes = self.get_mixture();
        
        llhs = []
        
        for i, node in enumerate(nodes):
            if node.num_local_data():
                llhs.append(node.num_local_data() * np.log(weights[i]) + node.data_log_likelihood() + node.parameter_log_prior()) 
        
        llh = np.sum(np.array(llhs))
        
        return llh
    
    def resample_tree_topology(self):
        post = np.log(np.random.rand()) + self.unnormalized_posterior()
        
        nodes = self.nodes
        
        if len(nodes) > 1:
            nodeAnum = np.random.randint(0, len(nodes))
            
            nodeBnum = np.random.randint(0, len(nodes))
            
            while nodeAnum == nodeBnum:
                nodeBnum = np.random.randint(0, len(nodes))
            
            def swap_nodes(nodeAnum, nodeBnum):
                def find_nodes(root, nodeNum, nodeA=False, nodeB=False):
                    node = root
                    
                    if nodeNum == nodeAnum:
                        nodeA = node
                    
                    if nodeNum == nodeBnum:
                        nodeB = node
                    
                    for child in root['children']:
                        nodeNum = nodeNum + 1
                        
                        (nodeA, nodeB, nodeNum) = find_nodes(child, nodeNum, nodeA, nodeB)
                    
                    return (nodeA, nodeB, nodeNum)
                
                (nodeA, nodeB, _) = find_nodes(self.root, nodeNum=0)
                
                paramsA = nodeA['node'].params
                
                dataA = set(nodeA['node'].data)
                
                mainA = nodeA['main']
            
                nodeA['node'].params = nodeB['node'].params
                
                for dataid in list(dataA):
                    nodeA['node'].remove_datum(dataid)
                
                for dataid in nodeB['node'].data:
                    nodeA['node'].add_datum(dataid)
                    
                    self.assignments[dataid] = nodeA['node']
                
                nodeA['main'] = nodeB['main']
                
                nodeB['node'].params = paramsA
                
                dataB = set(nodeB['node'].data)
                
                for dataid in dataB:
                    nodeB['node'].remove_datum(dataid)
                
                for dataid in dataA:
                    nodeB['node'].add_datum(dataid)
                
                    self.assignments[dataid] = nodeB['node']
                
                nodeB['main'] = mainA
                
            
            swap_nodes(nodeAnum, nodeBnum)
            
            self.resample_sticks()
            
            post_new = self.unnormalized_posterior()
            
            if(post_new < post):
                swap_nodes(nodeAnum, nodeBnum)
            
                self.resample_sticks()
            
            else:
                print "successful swap!!!"

    def resample_tree_topology_root(self):
        post = np.log(np.random.rand()) + self.unnormalized_posterior()
        
        nodes = self.nodes

        empty_root = False
        
        if len(nodes) > 1:
            if len(nodes[0].data) == 0:
                print 'swapping root'
                
                empty_root = True
                
                nodeAnum = 0
            
            else:
                nodeAnum = np.random.randint(0, len(nodes))
                
                candnum = range(len(nodes))
                
                candnum = candnum[:nodeAnum] + candnum[nodeAnum + 1:]
                
                tempidx = np.random.randint(0, len(nodes) - 1)
                
                nodeBnum = candnum[tempidx]
                
            def swap_nodes(nodeAnum, nodeBnum):
                def find_nodes(root, nodeNum, nodeA=False, nodeB=False):
                    node = root
                    
                    if nodeNum == nodeAnum:
                        nodeA = node
                    
                    if nodeNum == nodeBnum:
                        nodeB = node
                    
                    for child in root['children']:
                        nodeNum = nodeNum + 1
                        
                        (nodeA, nodeB, nodeNum) = find_nodes(child, nodeNum, nodeA, nodeB)
                    
                    return (nodeA, nodeB, nodeNum)
                
                (nodeA, nodeB, _) = find_nodes(self.root, nodeNum=0)
                
                paramsA = nodeA['node'].params
                
                dataA = set(nodeA['node'].data)
                
                mainA = nodeA['main']
            
                nodeA['node'].params = nodeB['node'].params
                
                for dataid in list(dataA):
                    nodeA['node'].remove_datum(dataid)
                
                for dataid in nodeB['node'].data:
                    nodeA['node'].add_datum(dataid)
                    
                    self.assignments[dataid] = nodeA['node']
                
                nodeA['main'] = nodeB['main']
                
                nodeB['node'].params = paramsA
                
                dataB = set(nodeB['node'].data)
                
                for dataid in dataB:
                    nodeB['node'].remove_datum(dataid)
                
                for dataid in dataA:
                    nodeB['node'].add_datum(dataid)
                    
                    self.assignments[dataid] = nodeB['node']
                
                nodeB['main'] = mainA

            if empty_root:
                print 'checking alternative root'
                
                nodenum = []
                
                for ii, nn in enumerate(nodes):
                    if len(nn.data) > 0:
                        nodenum.append(ii)
                
                post_temp = np.zeros(len(nodenum))
                
                for idx, nodeBnum in enumerate(nodenum):
                    print 'nodeBnum', nodeBnum
                    
                    print 'nodeAnum', nodeAnum
                    
                    swap_nodes(nodeAnum, nodeBnum)
                    
                    self.resample_sticks()
                    
                    post_new = self.unnormalized_posterior()
                    
                    post_temp[idx] = post_new
                    
                    if (post_new < post):
                        swap_nodes(nodeAnum, nodeBnum)
                        
                        self.resample_sticks()
                        
                        if nodeBnum == len(nodes) - 1:
                            print 'forced swapping'
                            
                            nodeBnum = post_temp.argmax() + 1
                            
                            swap_nodes(nodeAnum, nodeBnum)
                            
                            self.resample_sticks()
                            
                            self.resample_node_params()
                            
                            self.resample_stick_orders()
                    else:
                        print "successful swap!!!"
                        
                        self.resample_node_params()
                        
                        self.resample_stick_orders()
                        
                        break
            else:   
                swap_nodes(nodeAnum, nodeBnum)
                
                self.resample_sticks()
                
                post_new = self.unnormalized_posterior()
      
                if (post_new < post):
                    swap_nodes(nodeAnum, nodeBnum)
                
                    self.resample_sticks()
                
                else:
                    print "successful swap!!!"
                    
                    self.resample_node_params()
                    
                    self.resample_stick_orders()
    
    def to_networkx_graph(self):   
        edges = util.sticks_to_edges(self.root['sticks'])
        
        weights = np.diff(np.hstack([0.0, edges]))
        
        if len(weights) > 0:
            root_mass = weights[0] * self.root['main']
        
        # in case there is a problem with weights, as observed in some runs
        else:
            root_mass = -1
        
        g = nx.DiGraph()
        
        g.add_node('X', 
                   attr_dict={
                              'branch_length': float(self.root['node'].branch_length),
                              'mass' : float(root_mass),
                              'members' : ' '.join([str(x) for x in self.root['node'].data]),
                              'params' : ' '.join([str(x) for x in self.root['node'].params]),
                              'size' : len(self.root['node'].get_data())
                              })

        def descend(root, name, mass, g):
            total = 0.0
            
            edges = util.sticks_to_edges(root['sticks'])
            
            weights = np.diff(np.hstack([0.0, edges]))
            
            for i, child in enumerate(root['children']):
                child_name = '{0}_{1}'.format(name, i)
                
                child_mass = mass * weights[i] * child['main']
                
                g.add_node(child_name,
                           attr_dict={
                                      'branch_length' : float(child['node'].branch_length),
                                      'mass' : float(child_mass),
                                      'members' : ' '.join([str(x) for x in child['node'].data]),
                                      'params' : ' '.join([str(x) for x in child['node'].params]),
                                      'size' : len(child['node'].get_data()),
                                      })
                
                g.add_edge(name,
                           child_name,
                           attr_dict={
                                      'value' : len(child['node'].get_data()),
                                      'weight' : len(child['node'].get_data())
                                      })
#
                (tmp, g) = descend(child,
                                   child_name,
                                   mass * weights[i] * (1.0 - child['main']),
                                   g)
                
                total += child_mass + tmp
                
            return (total, g)

        (_, g) = descend(self.root, 'X', 1, g)
        
        return g

    def remove_empty_nodes(self):
        def descend(root):
            if len(root['children']) == 0:
                return

            while True:
                empty_nodes = filter(lambda i:
                                  len(root['children'][i]['node'].data) == 0,
                                  range(len(root['children'])))

                if len(empty_nodes) == 0:
                    break

                index = empty_nodes[0]
                
                cache_children = root['children'][index]['children']

                del root['children'][index]

                if len(cache_children) == 0:
                    continue
                
                else:

                    temp1 = root['children'][:index]

                    temp2 = root['children'][index:]

                    root['children'] = temp1 + cache_children + temp2
                    
                    root['sticks'] = np.zeros((len(root['children']), 1))
                    
            for child in root['children']:
                descend(child)
            
        descend(self.root)
        
        self.resample_sticks()
    
    def deepest_node_depth(self):
        def descend(root, depth):
            if len(root['children']) == 0:
                return depth
            
            deepest = depth
            
            for child in root['children']:
                hdepth = descend(child, depth + 1)
                
                if deepest < hdepth:
                    deepest = hdepth
            
            return deepest 
        
        return descend(self.root, 1)
    
    def get_width_distribution(self):
        def descend(root, depth, width_vec):
            width_vec[depth - 1] = width_vec[depth - 1] + 1
            
            if len(root['children']) == 0:
                return width_vec
            
            for child in root['children']:
                descend(child, depth + 1, width_vec)
            
            return width_vec
        
        width_vec = np.zeros(self.max_depth)
        
        return descend(self.root, 1, width_vec)
    
    def get_weight_distribtuion(self):
        def descend(root, mass, depth, mass_vec):
            edges = util.sticks_to_edges(root['sticks'])
            
            weights = np.diff(np.hstack([0.0, edges]))
            
            for i, child in enumerate(root['children']):
                mass_vec[depth] = mass_vec[depth] + mass * weights[i] * child['main']
                
                mass_vec = descend(child, mass * (1.0 - child['main']) * weights[i], depth + 1, mass_vec)
            
            return mass_vec 
        
        mass_vec = np.zeros(self.max_depth)
        
        edges = util.sticks_to_edges(self.root['sticks'])
        
        weights = np.diff(np.hstack([0.0, edges]))
        
        if len(weights) > 0 :
            mass_vec[0] = weights[0] * self.root['main']
            
            return descend(self.root, 1.0 - mass_vec[0], 1, mass_vec)
        
        else:
            return mass_vec
