from __future__ import division

import gzip
import os
import numpy as np
import pandas as pd

from bitphylogeny.model_spec import BitPhylogeny
from bitphylogeny.trace import Trace
from bitphylogeny.tssb import TSSB
from bitphylogeny.util import sigmoid

def run_analysis(args):
    data = pd.read_csv(args.in_file, compression='gzip', index_col='cell_id', sep='\t')

    if args.seed is not None:
        np.random.seed(args.seed)
    
    root = BitPhylogeny(dims=data.shape[1], mode=args.mode)
    
    tssb = TSSB(dp_alpha=2.0, dp_gamma=3e-1, alpha_decay=0.1, root_node=root, data=data.values)
    
    trace = Trace(data.index, 
                  data.columns, 
                  args.out_dir,
                  big_node_threshold=args.big_node_threshold,
                  reduce_storage=args.reduce_storage)

    print 'Analysing file: {0}'.format(args.in_file)
    print 'Number of cells: {0}'.format(data.shape[0])
    print 'Number of events: {0}'.format(data.shape[1])
    
    max_posterior = float('-inf')
    
    for sample in range(-args.burnin, args.num_samples):
        if sample == 0:
            print 'Burnin finished, collecting samples.'

        tssb.resample_assignments()

        tssb.cull_tree()

        tssb.resample_node_params()

        root.resample_hyper_parameters()

        tssb.resample_sticks()

        tssb.resample_stick_orders()

        tssb.resample_tree_topology_root()

        # tssb.resample_hyper_parameters(dp_alpha=True, alpha_decay=True, dp_gamma=True)
       
        if sample < 0:
            continue

        if sample % args.thin == 0:
            trace.update(root, tssb)
            
            if max_posterior < tssb.unnormalized_posterior():
                max_posterior = tssb.unnormalized_posterior()
                
                print '{0} is best per-data complete data likelihood so far.'.format(max_posterior / data.shape[0])

        if sample % (10 * args.thin) == 0:
            print_sampler_information(args.big_node_threshold, root, sample, trace, tssb)
            
    trace.close()

def cluster_data(args):
    file_name = os.path.join(args.analysis_out_dir, 'trace.h5')
    
    labels = _cluster_data(file_name)
    
    with gzip.GzipFile(args.out_file, 'w') as fh:
        labels.to_csv(fh, index_label='cell_id', sep='\t')

def _cluster_data(file_name):
    from pydp.cluster import cluster_with_mpear
    
    df = pd.read_hdf(file_name, 'labels')
    
    df = df.pivot(index='iter', columns='cell_id', values='value')
    
    labels = cluster_with_mpear(df)
    
    return pd.DataFrame({'cluster' : labels}, index=pd.Index(df.columns, name='cell_id'))

def compute_cluster_genotypes(args):
    file_name = os.path.join(args.analysis_out_dir, 'trace.h5')
    
    labels = _cluster_data(file_name)
    
    labels.index = labels.index.astype(str)
   
    if args.mean_params:
        params = pd.read_hdf(file_name, 'node_params_sigmoid')

    else:
        params = pd.read_hdf(file_name, 'node_params')
        
        params = sigmoid(params)
        
        params = params.mean(axis=2)

    params.index = params.index.astype(str)
    
    df = pd.concat([labels, params], axis=1, join='inner')
    
    df = df.groupby('cluster').mean()

    with gzip.GzipFile(args.out_file, 'w') as fh:
        df.to_csv(fh, sep='\t')
        
def print_sampler_information(big_node_threshold, root, sample, trace, tssb):
    (weights, nodes) = tssb.get_mixture()
    
    num_big_nodes = int(np.sum(np.array(weights) > big_node_threshold))
    
    message = (sample,
               len(nodes),
               tssb.complete_data_log_likelihood(),
               root.mu,
               root.std,
               'big nodes: {0}'.format(num_big_nodes),
               np.diag(root.rate_matrix),
               np.mean(np.array([x.branch_length for x in tssb.assignments])),
               tssb.dp_alpha,
               tssb.dp_gamma,
               tssb.alpha_decay)
            
    print ' '.join([str(x) for x in message])
