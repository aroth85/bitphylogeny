'''
Created on 2015-05-13

@author: Andrew Roth
'''
from __future__ import division

from scipy.stats import itemfreq as item_freq

import cPickle
import networkx as nx
import numpy as np
import os
import pandas as pd

from bitphylogeny.util import object_to_label, safe_make_dir, sigmoid, write_trace_to_tsv

class Trace(object):
    def __init__(self, cell_ids, event_ids, out_dir, big_node_threshold=0.1, reduce_storage=False):
        self.tree_dir = os.path.join(out_dir, 'trees')
        
        safe_make_dir(out_dir)
    
        safe_make_dir(self.tree_dir)
        
        self.big_node_threshold = big_node_threshold
        
        self.cell_ids = cell_ids
        
        self.event_ids = event_ids
        
        self.iter = 0
        
        self.reduce_storage = reduce_storage
        
        trace_file = os.path.join(out_dir, 'trace.h5')
        
        self.trace = pd.HDFStore(trace_file, 'w', complevel=9, complib='blosc')

        self._init_map_trees()
        
        # Initialise trace tables

        self._init_misc()
#                
        self._init_table('branch_lengths', 'cell_id')
        
        self._init_table('labels', 'cell_id')
        
        self._init_table('node_depth', 'cell_id')
         
        self._init_table('root_param', 'event_id')
         
        self._init_table('tssb_mass', 'component_id')
                 
        self._init_table('tssb_mass', 'component_id')
         
        self._init_node_params()
     
    def close(self):
        self._write_trees()
         
        if self.reduce_storage:
            self.trace['node_params'] = self.trace['node_params'] / self.iter
             
            self.trace['node_params_sigmoid'] = self.trace['node_params_sigmoid'] / self.iter 
         
        self.trace.close()
    
    def update(self, root, tssb):
        (weights, nodes) = tssb.get_mixture()
        
        num_big_nodes = int(np.sum(np.array(weights) > self.big_node_threshold))
                
        self._update_branch_lengths(tssb)
        
        self._update_labels(nodes, tssb)
         
        self._update_map_trees(num_big_nodes, tssb)
         
        self._update_misc(nodes, num_big_nodes, root, tssb)
         
        self._update_node_depth(tssb)
         
        self._update_node_params(tssb)
         
        self._update_root_params(tssb)
         
        self._update_tssb_params(tssb)
         
        self.iter += 1
    
    def _init_map_trees(self):
        self.map_trees = {'iter' : {},
                          'max_posterior' : {},
                          'tssb' : {}}
    
    def _init_node_params(self):
        if self.reduce_storage:
            df = np.zeros((len(self.cell_ids), len(self.event_ids)))
            
            df = pd.DataFrame(df, index=self.cell_ids, columns=self.event_ids)
            
            self.trace.append('node_params_sigmoid', df.copy())
        
        else:
            df = pd.Panel(items=self.event_ids, major_axis=self.cell_ids)
        
        self.trace.append('node_params', df)
    
    def _init_misc(self):
        cols = ['cd_llh', 'nodes', 'big_nodes', 'unnormpost', 'depth', 'base_value', 'std']
        
        df = pd.DataFrame(columns=cols)
        
        self.trace.append('misc', df)
    
    def _init_table(self, name, col_name):
        df = pd.DataFrame(columns=['iter', col_name, 'value'])
        
        self.trace.append(name, df)
   
    def _update_branch_lengths(self, tssb):
        values = np.array([x.branch_length for x in tssb.assignments])
        
        self._update_table('branch_lengths', 'cell_id', self.cell_ids, values)
    
    def _update_labels(self, nodes, tssb):
        values = object_to_label(tssb.assignments, nodes)
        
        self._update_table('labels', 'cell_id', self.cell_ids, values)
        
    def _update_map_trees(self, num_big_nodes, tssb):
        unnorm_post = tssb.unnormalized_posterior()
        
        if (num_big_nodes not in self.map_trees['tssb']) or (unnorm_post > self.map_trees['max_posterior'][num_big_nodes]):
            self.map_trees['iter'][num_big_nodes] = self.iter
            
            self.map_trees['max_posterior'][num_big_nodes] = unnorm_post

            self.map_trees['tssb'][num_big_nodes] = cPickle.dumps(tssb)
            
    def _update_misc(self, nodes, num_big_nodes, root, tssb):
        unnorm_post = tssb.unnormalized_posterior()
                                
        misc = {'base_value' : root.mu,
                'big_nodes' : num_big_nodes,
                'cd_llh' : tssb.complete_data_log_likelihood(),
                'depth' : tssb.deepest_node_depth(),
                'nodes' : len(nodes),
                'std' : root.std,
                'unnormpost' : unnorm_post}
        
        misc = pd.DataFrame(misc)
        
        misc.index = [self.iter, ]
        
        self.trace.append('misc', misc)
        
    def _update_node_depth(self, tssb):
        values = np.array([x.depth for x in tssb.assignments])
        
        self._update_table('node_depth', 'cell_id', self.cell_ids, values)
        
    def _update_node_params(self, tssb):
        values = np.array([x.params for x in tssb.assignments])
        
        if self.reduce_storage:
            self.trace['node_params'] = self.trace['node_params'] + values
            
            self.trace['node_params_sigmoid'] = self.trace['node_params_sigmoid'] + sigmoid(values)
            
        else:
            df = pd.Panel(values.T[:, :, np.newaxis],
                          items=self.event_ids,
                          major_axis=self.cell_ids,
                          minor_axis=[self.iter, ])
       
            self.trace.append('node_params', df)
        
    def _update_root_params(self, tssb):
        values = tssb.root['node'].params
        
        self._update_table('root_params', 'event_id', self.event_ids, values)
        
    def _update_table(self, name, col_name, columns, values):
        df = pd.DataFrame(values, index=columns)
         
        df = df.T
            
        df = df.unstack().reset_index()
        
        df.columns = col_name, 'iter', 'value'
        
        df['iter'] = self.iter
        
        self.trace.append(name, df)
    
    def _update_tssb_params(self, tssb):
        columns = range(tssb.max_depth)
        
        values = tssb.get_weight_distribtuion()
        
        self._update_table('tssb_mass', 'component_id', columns, values)
        
        values = tssb.get_width_distribution()
        
        self._update_table('tssb_width', 'component_id', columns, values)
    
    def _write_trees(self):
        misc = self.trace['misc']
        
        # Write fequency table
        nodes_tabular = item_freq(misc['big_nodes'])
        
        nodes_tabular = nodes_tabular.astype('float')
    
        nodes_tabular[:, 1] = nodes_tabular[:, 1] / nodes_tabular[:, 1].sum()
    
        header = ['unique_node_num', 'freq']
        
        write_trace_to_tsv(os.path.join(self.tree_dir, 'tree_freq.tsv.gz'), nodes_tabular, header)
        
        for num_big_nodes in self.map_trees['tssb']:
            node_fit = cPickle.loads(self.map_trees['tssb'][num_big_nodes])
            
            file_name = 'nodes_{0}.graphml'.format(num_big_nodes)
            
            file_name = os.path.join(self.tree_dir, file_name)
            
            g = node_fit.to_networkx_graph()
            
            g.graph['index'] = self.map_trees['iter'][num_big_nodes]
            
            nx.write_graphml(g, file_name)
