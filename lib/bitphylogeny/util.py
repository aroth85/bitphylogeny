from collections import OrderedDict
from scipy.special import gammaln as log_gamma

import gzip
import numpy as np
import os
import pandas as pd
import shutil

def safe_make_dir(dir_path):
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)
    
    else:
        shutil.rmtree(dir_path)
    
        os.makedirs(dir_path)

def object_to_label(obj, nodes):
    labels = []
    
    for item in obj:
        for ii, nn in enumerate(nodes):
            if item == nn:
                labels.append(ii)
    
    return np.array(labels)

def write_trace_to_tsv(file_name, trace, header=None):
    trace = np.atleast_2d(np.squeeze(np.array(trace)))

    if header is None:
        df = pd.DataFrame(trace)
    
    else:
        df = pd.DataFrame(OrderedDict(zip(header, trace.T)))
    
    with gzip.GzipFile(file_name, 'w') as fh:
        df.to_csv(fh, index=False, sep='\t')

def sticks_to_edges(sticks):
    return 1.0 - np.cumprod(1.0 - sticks)

def log_beta_pdf(x, a, b):
    x = np.atleast_1d(x)
    
    if any(x == 0.0) or any(x == 1.0):
            return float('-inf')
    
    if any(x < 0.0) or any(x > 1.0):
        print x
    
    return log_gamma(a + b) - log_gamma(a) - log_gamma(b) + (a - 1.0) * np.log(x) + \
      (b - 1.0) * np.log(1.0 - x) 

def bound_beta(a, b):
    return (1.0 - np.finfo(np.float64).eps) * (np.random.beta(a, b) - 0.5) + 0.5
    
def log_dirichlet_normalisation(a):
    return np.sum(log_gamma(a)) - log_gamma(np.sum(a))

def log_dirichlet_pdf(p, a):
    return -log_dirichlet_normalisation(a) + np.sum((a - 1) * np.log(p))

def sigmoid(x):
    res = 1.0 / (1.0 + np.exp(-x))
    
    return res

def slice_sample(init_x, log_prob, compwise=False, max_steps_out=1000, sigma=1.0, step_out=True, verbose=False):
    
    def direction_slice(direction, init_x):
        
        def dir_logprob(z):
            return log_prob(direction * z + init_x)
    
        upper = sigma * np.random.rand()
        
        lower = upper - sigma
        
        llh_s = np.log(np.random.rand()) + dir_logprob(0.0)
    
        l_steps_out = 0
        
        u_steps_out = 0
        
        if step_out:
            while dir_logprob(lower) > llh_s and l_steps_out < max_steps_out:
                l_steps_out += 1
    
                lower -= sigma
            
            while dir_logprob(upper) > llh_s and u_steps_out < max_steps_out:
                u_steps_out += 1
            
                upper += sigma
            
        steps_in = 0
        
        while True:
            steps_in += 1
            
            new_z = (upper - lower) * np.random.rand() + lower
            
            new_llh = dir_logprob(new_z)
            
            if np.isnan(new_llh):
                print new_z, direction * new_z + init_x, new_llh, llh_s, init_x, log_prob(init_x)
                
                raise Exception("Slice sampler got a NaN")
            
            if new_llh > llh_s:
                break
            
            elif new_z < 0:
                lower = new_z
            
            elif new_z > 0:
                upper = new_z
            
            else:
                raise Exception("Slice sampler shrank to zero!")

        if verbose:
            print "Steps Out:", l_steps_out, u_steps_out, " Steps In:", steps_in

        return new_z * direction + init_x
    
    dims = init_x.shape[0]
    
    if compwise:
        ordering = range(dims)
        
        np.random.shuffle(ordering)
        
        cur_x = init_x.copy()
        
        for d in ordering:
            direction = np.zeros((dims))
        
            direction[d] = 1.0
            
            cur_x = direction_slice(direction, cur_x)
        
        return cur_x
        
    else:
        direction = np.random.randn(dims)
    
        direction = direction / np.sqrt(np.sum(direction ** 2))
        
        return direction_slice(direction, init_x)