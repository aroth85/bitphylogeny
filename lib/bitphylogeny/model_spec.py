from scipy.misc import logsumexp as log_sum_exp

import numpy as np
import scipy as sp

from bitphylogeny.node import Node

import bitphylogeny.util as util

def inverse_sigmoid(z):
    return np.log(z) - np.log(1.0 - z)

def log_sigmoid(x):
    return -np.log(1.0 + np.exp(-x))

def sigmoid(x):
    return 1.0 / (1.0 + np.exp(-x))

def log_laplace_pdf(x, m, std):
    return -np.log(2 * std) - abs(x - m) / std

def log_normal_pdf(x, m, std):
    return -0.5 * np.log(2 * np.pi) - np.log(std) - 0.5 * ((x - m) / std) ** 2

def bound_prob(x):
    return (1.0 - np.finfo(np.float64).eps) * (x - 0.5) + 0.5

def get_weights(parent_params, ratemat):
    mapper = (parent_params > np.ones(len(parent_params)))
    
    weights = sp.linalg.expm2(ratemat)
    
    m1 = weights[1, 1]
    
    m2 = weights[0, 1]
    
    if m1 == 0.0:
        m1 = bound_prob(m1)
    
    if m2 == 0.0:
        m2 = bound_prob(m2)
    
    return m1 * mapper + ~mapper * m2

def laplace_mixture_prior(params, base_value, std, ratemat):
    weights = get_weights(params, ratemat)    
    
    mapper = (np.random.rand(len(weights)) < weights)
    
    m1 = np.random.laplace(base_value * np.ones(len(weights)), std * np.ones(len(weights)))
    
    m2 = np.random.laplace(-base_value * np.ones(len(weights)), std * np.ones(len(weights)))
    
    return m1 * mapper + ~mapper * m2

def log_laplace_mixture_pdf(x, m, std, parent_params, ratemat):
    weights = get_weights(parent_params, ratemat)
    
    logw1 = np.log(weights)
    
    mapper = (logw1 != 0)
    
    lap1 = log_laplace_pdf(x, m[0], std)
    
    lap2 = log_laplace_pdf(x, m[1], std)
    
    ll1 = logw1[mapper] + lap1[mapper]
    
    logw2 = np.log(1 - weights[mapper])
    
    ll2 = logw2 + lap2[mapper]
    
    ll = np.sum(log_sum_exp(np.vstack([ll1, ll2]), 0))
    
    ll = ll + np.sum(lap1[~mapper])
    
    return ll

def log_root_prior_pdf(x, m, std, ratemat):
    ll = log_laplace_pdf(x, -5.0, 0.5) 
    
    return np.sum(ll)

def root_prior_rvs(base_value, std, dims):
    m2 = np.random.laplace(-5.0 * np.ones(dims), 0.5 * np.ones(dims))
    
    return m2

class BitPhylogeny(Node):
    min_mu = 2.0

    max_mu = 7.0
    
    min_std = 0.3
    
    max_std = 20.0
    
    max_param = 10.0
    
    min_branch_length = 0.0
    
    max_branch_length = 1.0

    def __init__(self,
                 dims=1,
                 mode='methylation',
                 mu=5.0,
                 parent=None,
                 ratemat=32.0 / 7.0 * np.array([[-1.0 / 8.0, 1.0 / 8.0], [7.0 / 8.0, -7.0 / 8.0]]),
                 std=1.0,
                 tssb=None):
        
        super(BitPhylogeny, self).__init__(parent=parent, tssb=tssb)

        if parent is None:
            self.mode = mode
            
            self.dims = dims
            
            self.depth = 0.0
            
            self._base_value = mu * np.ones(1)
            
            self._std = std * np.ones(1)
            
            self._ratemat = ratemat
            
            self.branch_length = np.zeros(1) 
            
            self.params = root_prior_rvs(self._base_value, self._std, dims)
            
        else:
            self.mode = parent.mode
            
            self.dims = parent.dims
            
            self.depth = parent.depth + 1.0
            
            self.branch_length = np.random.uniform(0, 1) * np.ones(1)
            
            self.params = laplace_mixture_prior(parent.params,
                                                self.mu,
                                                self.std,
                                                self.rate_matrix * self.branch_length)

            if any(abs(self.params) > self.max_param):
                for index, x in enumerate(self.params):
                    if abs(x) > self.max_param:
                        if x > 0.0:
                            self.params[index] = self.max_param * 0.99999
                        
                        else :
                            self.params[index] = -self.max_param * 0.99999
                                   
        self._cache_sigmoidln()

    def _cache_sigmoidln(self):
        self._log_sigmoid = log_sigmoid(self.params[np.newaxis, :])
        
        self._negative_log_sigmoid = log_sigmoid(-self.params[np.newaxis, :])
    
    @property
    def mu(self):
        if self.parent() is None:
            return self._base_value
        
        else:
            return self.parent().mu
    
    @property
    def std(self):
        if self.parent() is None:
            return self._std
        
        else:
            return self.parent().std
    
    @property
    def rate_matrix(self):
        if self.parent() is None:
            return self._ratemat
        
        else:
            return self.parent().rate_matrix

    def sample(self, args):
        num_data = args['num_data'] if args.has_key('num_data') else 1
        
        return np.random.rand(num_data, self.dims) < sigmoid(self.params[np.newaxis, :])
    
    def resample_parameters(self):
        mode = self.mode
        
        data = self.get_data()
        
        counts = np.sum(data, axis=0)
        
        num_data = data.shape[0]
        
        mm = (self.mu, -self.mu)
        
        est_std = self.std
        
        ratemat = self.rate_matrix
        
        est_branch = self.branch_length
        
        def log_posterior(params):
            if any(params >= self.max_param) or any(params <= -self.max_param):
                llh = float('-inf')

            if self.parent() is None:
                llh = log_root_prior_pdf(params, self.mu, est_std, ratemat)
            
            else:
                llh = log_laplace_mixture_pdf(params, mm, est_std, self.parent().params, ratemat * est_branch)

            if mode == 'methylation':
                llh = llh + np.sum(counts * log_sigmoid(params)) + np.sum((num_data - counts) * log_sigmoid(-params))
            
            elif num_data > 0:
                llh = llh + np.nansum(data * log_sigmoid(params) + (1.0 - data) * log_sigmoid(-params))

            for child in self.children():
                llh = llh + log_laplace_mixture_pdf(child.params, mm, est_std, params, ratemat * child.branch_length)

            return llh

        self.params = util.slice_sample(self.params,
                                        log_posterior,
                                        compwise=False,
                                        sigma=5.0,
                                        step_out=True)
        
        self._cache_sigmoidln()

        def log_posterior_branch_length(branch_length):
            if any(branch_length < self.min_branch_length) or any(branch_length > self.max_branch_length):
                return float('-inf')

            params = self.params
            
            parent_params = self.parent().params    
            
            llh = log_laplace_mixture_pdf(params, mm, est_std, parent_params, ratemat * branch_length)

            return llh
        
        if self.parent() is None:
            self.branch_length = np.zeros(1)
        
        else:
            self.branch_length = util.slice_sample(self.branch_length,
                                                   log_posterior_branch_length,
                                                   compwise=False,
                                                   sigma=1.0,
                                                   step_out=True)

    def resample_hyper_parameters(self):
        if self.parent() is not None:
            raise Exception("Can only update hyper-parameters from root!")

        def estimate_rate_matrix(root):
            nodes = root.tssb.nodes
            
            params = []
            
            for nn in nodes:
                pp = nn.params
                
                pp = (pp > np.ones(len(pp)))
                
                params.append(pp)

            params = np.array(params)
            
            rate1 = np.mean(np.sum(params, axis=1).astype(np.float64) / len(pp))

            if (rate1 == 0.0) or (rate1 == 1.0):
                rate1 = bound_prob(rate1)
                
            rate2 = 1 - rate1

            if self.mode == "methylation":
                scalar = 1.0 / (2 * rate1 * rate2)
            
                ratemat = scalar * np.array([[-rate1, rate1], [rate2, -rate2]])
            
            else:
                scalar = 1.0 / (rate1 * rate2)
                
                ratemat = scalar * np.array([[-rate1, rate1], [0.0, 0.0]])
                
            return ratemat

        self._ratemat = estimate_rate_matrix(self)
                
        def log_posterior_mu(mu):
            if any(mu < self.min_mu) or any(mu > self.max_mu):
                return float('-inf')
            
            def log_likelihood(root):
                llh = 0.0
            
                for child in root.children():
                    llh = llh + log_laplace_mixture_pdf(child.params,
                                                        (mu, -mu),
                                                        child.std,
                                                        root.params,
                                                        child.rate_matrix * child.branch_length)
                    
                    llh = llh + log_likelihood(child)
                
                return llh
            
            return log_likelihood(self) 

        self._base_value = util.slice_sample(self._base_value,
                                             log_posterior_mu,
                                             compwise=True,
                                             sigma=1.0,
                                             step_out=True)

        def log_posterior_std(est_std):
            if any(est_std < self.min_std) or any(est_std > self.max_std):
                return float('-inf')
            
            def log_likelihood(root):
                llh = 0.0
                
                for child in root.children():
                    llh = llh + log_laplace_mixture_pdf(child.params,
                                                        (child.mu, -child.mu),
                                                        est_std,
                                                        root.params,
                                                        child.rate_matrix * child.branch_length)
                    
                    llh = llh + log_likelihood(child)
                
                return llh
            
            return log_likelihood(self) 

        self._std = util.slice_sample(self._std,
                                      log_posterior_std,
                                      compwise=True,
                                      sigma=1.0,
                                      step_out=True)
        
    def log_prob(self, x):
        return np.nansum(x * self._log_sigmoid + (1.0 - x) * self._negative_log_sigmoid)

    def complete_log_prob(self):
        return self.log_prob(self.get_data())

    def parameter_log_prior(self):
        if self.parent() is None:
            llh = log_root_prior_pdf(self.params,
                                     self.mu,
                                     self.std,
                                     self.rate_matrix)
        
        else:
            llh = log_laplace_mixture_pdf(self.params,
                                          (self.mu, -self.mu),
                                          self.std,
                                          self.parent().params,
                                          self.rate_matrix * self.branch_length)
        
        return llh
