from distutils.core import setup
 
setup(
      name='BitPhylogeny',
      version='0.3.2',
      description='Bayesian inference of intra tumor phylogenies.',
      author='Ke Yuan and Thomas Sakoparnig',
      author_email='ke.yuan.09@gmail.com',
      url='https://bitbucket.org/ke_yuan/bitphylogeny',
      package_dir={'': 'lib'},
      packages=['bitphylogeny',],
      scripts=['bin/BitPhylogeny']
     )
